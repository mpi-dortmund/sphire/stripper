__version__ = "0.0.5"

"""
0.0.1 first running version. ridge-detection (1.0.0) package has to be pre installed
0.0.2 it is based on ridge_detection 1.1.0, that is installed automatic via setup.py
0.0.3 Collect the data via GUI instead of json file
0.0.4 Apply the binary mask is now possible
0.0.5 added the descriptions in the info parameter GUIs

"""


"""
MIT License

Copyright (c) 2020 Max Planck Institute of Molecular Physiology

Author: Luca Lusnig (luca.lusnig@mpi-dortmund.mpg.de)
Author: Thorsten Wagner (thorsten.wagner@mpi-dortmund.mpg.de)
Author: Markus Stabrin (markus.stabrin@mpi-dortmund.mpg.de)
Author: Fabian Schoenfeld (fabian.schoenfeld@mpi-dortmund.mpg.de)
Author: Tapu Shaikh (tapu.shaikh@mpi-dortmund.mpg.de)
Author: Adnan Ali (adnan.ali@mpi-dortmund.mpg.de)


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from datetime import datetime
import mrcfile
from multiprocessing import cpu_count

from stripper.params import Params
from ridge_detection.helper import normalizeImg,INTEGER_8BIT_MIN,INTEGER_8BIT_MAX
from stripper.helper import resize_img, param_json_for_ridge_detection,saturation
from stripper.maskStackCreator import MaskStackCreator
from stripper.filamentEnhancer import enhance_images
from stripper.filamentFilter import filterLines,asarray
from stripper.filamentDetector import filamentDetectorWorker
from stripper.box import placeBoxesPainter
from numpy import mean,std,uint16
from PIL import Image,ImageDraw

from stripper.gui_helper import show_images

def get_RD_output(img,lines,junctions):
    """
    I basically rewrote the ridge_detection.helper.displayContour.
    It plot the lines and the junction on the input img
    :param img: img as numpy array
    :param lines: detected lines as list
    :param junctions: detected junctions as list
    :return: result image
    """
    size_ray = 1
    im2=Image.fromarray(img).convert('RGB')
    im=im2.load()
    """ plot the lines with red colour"""
    for l in lines:
        for i,j in zip(l.col,l.row):
            im[int(i), int(j)] =  (255, 0, 0)

    """ draw a circle (with ray SIZE_RAY_JUNCTION) centered in each junctions"""
    for junction in junctions:
        draw = ImageDraw.Draw(im2)
        draw.ellipse((int(junction.x) - size_ray, int(junction.y) - size_ray, int(junction.x) + size_ray, int(junction.y) + size_ray), fill = 'blue')
    return asarray(im2)




# it is basically the run in the PipelineRunner.java
def run(param,darkline,overlap,save_out_on_disk):
    """
    :param param: dict containing the parameters obtained via GUI for the stripper configuration
    :param darkline: boolean value to set the DarkLine Param for the ridge detection tool
    :param overlap: boolean value to set the overlap Param for the ridge detection tool
    :param save_out_on_disk: save the output image of ridge detection and str
    """



    print("START:",datetime.now())
    params=Params(param)

    try:
        img=mrcfile.open(params.config_path_to_file).data
    except ValueError:
        img=asarray(Image.open(params.config_path_to_file))

    if params.resize is not None:
        img=resize_img(img=img,resize=params.resize)
    if params.convert8bit is True:
        img=normalizeImg(img=img,new_max=INTEGER_8BIT_MAX,new_min=INTEGER_8BIT_MIN,return_Aslist=False)

    """ START helicalPicker->gui->PipelineRunner.java --> run() """
    """ STEP 1: enhance images"""
    print(str(datetime.now())+" STEP 1: enhance images ")
    maskcreator=MaskStackCreator(filament_width=params.enhancerContext['filament_width'],
                                 mask_size=img.shape[0],
                                 mask_width=params.enhancerContext['mask_width'],
                                 angle_step=params.enhancerContext['angle_step'],
                                 interpolation_order = 3,
                                 bright_background=False)

    """ the input images has to be always a list"""
    if isinstance(img,list) is False:
        img=[img]
    enhanced_imgs = enhance_images(input_images=img, maskcreator=maskcreator, num_cpus=cpu_count())

    """ for the stripper I need the 'max_value' images"""
    enhanced_imgs=[img["max_value"] for img in enhanced_imgs]

    """ If the "Detection.Equalize" flag is True, the enhanced images have to:
        1) normalize via mean and std 
        2) saturate max to 5
    """
    if params.enhancerContext["equalize"] is True:
        enhanced_imgs = [(img-mean(img))/std(img) for img in enhanced_imgs]
        enhanced_imgs = [saturation(img=img,max_value=5) for img in enhanced_imgs]

    enhanced_imgs_for_plot = [normalizeImg(enhanced_img, return_Aslist=False).astype(dtype=uint16) for enhanced_img in enhanced_imgs]


    # todo: 1) insert 'assistant' option ... in case you cannot use 'filamentDetectContext=params.detectorContext'
    # todo: 2) make optional show images()

    params_ridge_detection=param_json_for_ridge_detection(sigma=params.detectorContext["sigma"],
                                   lower_th=params.detectorContext["thresholdRange"]["lower_threshold"],
                                   upper_th=params.detectorContext["thresholdRange"]["upper_threshold"],
                                   max_l_len=0, min_l_len=0,
                                   darkLine=darkline, doCorrecPosition=True, doEstimateWidth=True, doExtendLine=True,
                                   overlap=overlap)



    for enhanced_img,enhanced_img_for_plot,input_img in zip (enhanced_imgs,enhanced_imgs_for_plot,img):
        """ STEP 2: detect filaments"""
        print(str(datetime.now())+" STEP 2: detect filaments")
        lines_in_enhanced_img,junctions_in_enhanced_img = filamentDetectorWorker(enhanced_img=enhanced_img,
                                                            params_ridge_detection=params_ridge_detection)

        rd_out=get_RD_output(enhanced_img_for_plot,lines_in_enhanced_img,junctions_in_enhanced_img)

        """ STEP 3: filament filaments"""
        print(str(datetime.now())+" STEP 3: filament filaments")

        filtered_lines= filterLines(lines=lines_in_enhanced_img,
                                    filamenFilter_context=params.filterContext,
                                    input_image=input_img,
                                    response_map=enhanced_img,
                                    junctions=junctions_in_enhanced_img)

        striper_out = asarray(placeBoxesPainter(lines=filtered_lines, target_img=input_img, placing_context=params.placing_context,box_top_left=False))

        show_images(rd_out,striper_out,save_out_on_disk,params.filterContext["mask"] is not None )

    print("END:", datetime.now())

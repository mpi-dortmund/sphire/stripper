"""
MIT License

Copyright (c) 2020 Max Planck Institute of Molecular Physiology

Author: Luca Lusnig (luca.lusnig@mpi-dortmund.mpg.de)
Author: Thorsten Wagner (thorsten.wagner@mpi-dortmund.mpg.de)
Author: Markus Stabrin (markus.stabrin@mpi-dortmund.mpg.de)
Author: Fabian Schoenfeld (fabian.schoenfeld@mpi-dortmund.mpg.de)
Author: Tapu Shaikh (tapu.shaikh@mpi-dortmund.mpg.de)
Author: Adnan Ali (adnan.ali@mpi-dortmund.mpg.de)


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


from PIL import Image
from stripper.helper import createSliceRange,resize_img
from stripper.filamentDetector import createFilamentDetectorContext,filamentWidthToSigma
from stripper.filamentEnhancer import createFilamentEnhancerContext
from stripper.filamentFilter import createFilamentFilterContext
from stripper.box import createBoxPlacingContext
from math import sqrt
from  mrcfile import open as mrcfile_open
from numpy import asarray


class Params:
    def __init__(self, values):
        self.config_path_to_file = values["path_to_file"]
        self.slice_range=createSliceRange(slice_from=0,slice_to=0)          #todo we will change these values when I'll open the stack, if it is a stack

        self.detectorContext=createFilamentDetectorContext(sigma=filamentWidthToSigma(values["Detection"]["Filament_width"]),
                                                           lower_threshold=values["Detection"]["Lower_Threshold"],
                                                           upper_threshold=values["Detection"]["Upper_Threshold"])

        self.enhancerContext=createFilamentEnhancerContext(filament_width=values["Detection"]["Filament_width"],
                                                           mask_width=values["Detection"]["Mask_width"],
                                                           angle_step=2,
                                                           equalize=values["Detection"]["Equalize"])

        self.resize = (values["General"]["Resize"], values["General"]["Resize"]) if "Resize" in values["General"] else None

        mask = None
        if values["Filtering"]["Custom_mask"] is not False:
            try:
                mask = mrcfile_open(values["Filtering"]["Custom_mask"]).data
            except ValueError:
                mask = asarray(Image.open(values["Filtering"]["Custom_mask"]))

        if mask is not None and  self.resize is not None:
            mask = resize_img(img=mask, resize=self.resize)

        min_filament_distance= sqrt( 2*(values["Filtering"]["Allowed_box_overlapping"]*values["General"]["Box_size"])*(values["Filtering"]["Allowed_box_overlapping"]*values["General"]["Box_size"])  ) /2

        self.filterContext=createFilamentFilterContext(min_number_boxes = values["Filtering"]["Min_number_of_boxes"],
                                    min_line_straightness = values["Filtering"]["Min_straightness"],
                                    window_width_straightness = values["Filtering"]["Window_size"],
                                    removement_radius = int(values["General"]["Box_size"]/2),
                                    fit_distribution = False,
                                    sigma_min_response = values["Filtering"]["Sigma_min_response"],
                                    sigma_max_response = values["Filtering"]["Sigma_max_response"],
                                    min_filament_distance = min_filament_distance,
                                    border_diameter = int(values["General"]["Box_size"]/2),
                                    double_filament_insensitivity = 1-values["Filtering"]["Sensitivity"],
                                    userFilters = None,
                                    box_size = values["General"]["Box_size"],
                                    box_distance = values["General"]["Box_distance"],
                                    mask = mask)  #.astype(bool) has to be a boolean image. I have to check if it is possible avoid to cast that here

        #todo: the slicePosition has to be changed in the stack position ... of course if I'll decide to use a stack
        self.placing_context=createBoxPlacingContext(slicePosition =1,box_size = values["General"]["Box_size"],
                                                     box_distance = values["General"]["Box_distance"],
                                                     place_points = values["General"]["place_points"])

        self.convert8bit=values["General"]["Convert_to_8bit"] if "Convert_to_8bit" in values["General"] else False

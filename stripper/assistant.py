from random import random
from stripper.filamentDetector import createDetectionThresholdRange,filamentWidthToSigma,createFilamentDetectorContext
from ridge_detection import lineDetector
from stripper.helper import param_json_for_ridge_detection,saturation
from stripper.filamentEnhancer import createFilamentEnhancerContext,enhance_images
from stripper.maskStackCreator import MaskStackCreator
from multiprocessing import cpu_count
from numpy import mean,std

def next_rand_upper(m=0,M=20):
    """
    Generate a random number between m and M
    """
    return m+random()*(M-m)

# Parallel_Ridge_Optimizer.java function
def searchRange2(imgs,filament_width, n_try=10):
    """
    It is a search method to for the detection threshold (i.e.: lower_threshold and upper_threshold)
    :param imgs: List of two images on which the ridge detection tool will be applied
    :param filament_width: the width in pixels of the lines, selected via GUI.
    :param n_try: number of iteration of the search. FOr each iteration the 'ridge_detection.lineDetector' is called twice
    :return: DetectionThresholdRange dict
    """
    random_upper_max=20
    random_upper_min=0
    ub= next_rand_upper(m=random_upper_min,M=random_upper_max)   # We used the default value as in the java version
    sigma=filamentWidthToSigma(filament_width)
    for i in range(n_try-1):
        tot_lines=list()
        params_ridge_detection = param_json_for_ridge_detection(sigma=sigma,
                                                                lower_th=0,
                                                                upper_th=ub,
                                                                max_l_len=0,
                                                                min_l_len=0,
                                                                darkLine =False, doCorrecPosition=True, doEstimateWidth=False, doExtendLine=True, overlap=False)

        #todo: put it in a pool to speed up the code
        for img in imgs:
            ld=lineDetector.LineDetector(params=params_ridge_detection)
            tot_lines+=ld.get_lines(in_img=img)

        if len(tot_lines) == 0:
            random_upper_max = ub
        else:
            random_upper_min = ub

        ub = next_rand_upper(m=random_upper_min, M=random_upper_max)

    return createDetectionThresholdRange(lower_threshold=0,upper_threshold=random_upper_max)



def get_enhanced_substack(substack,filament_width,mask_width,numberOfProcessors):
    """
    Calculates the ehnanced image as in the run. Since we are in an heuristic. We have to calculate in more time in case of need
    """
    filament_enhanced_context=createFilamentEnhancerContext(filament_width=filament_width,
                                  mask_width=mask_width,
                                  angle_step=2,
                                  equalize=True)

    maskcreator = MaskStackCreator(filament_width=filament_enhanced_context['filament_width'],
                                   mask_size=substack[0].shape[0],
                                   mask_width=filament_enhanced_context['mask_width'],
                                   angle_step=filament_enhanced_context['angle_step'],
                                   interpolation_order=3,
                                   bright_background=False)

    enhanced_imgs = enhance_images(input_images=substack, maskcreator=maskcreator, num_cpus=numberOfProcessors)

    """ for the stripper I need the 'max_value' images"""
    enhanced_imgs = [img["max_value"] for img in enhanced_imgs]
    enhanced_imgs = [(img - mean(img)) / std(img) for img in enhanced_imgs]
    return [saturation(img=img, max_value=5) for img in enhanced_imgs]



def get_binary_substack(substack):
    """
    In pratica prende l'immagine bianca e ci colora di nero i filamenti. Compresi della larghezza della width.
    In pratica se consideri i filamenti come linee, serie di punti, ci disega attorno un quadrato di larghezza width data da selezione utente tramite gui
    :param substack:
    :return:
    """
    #todo: to implemet
    return list()



def optimize(substack,start_params,filament_width,mask_width,global_runs,local_runs,  normalize=False):
    """
    :param substack: list of images. I java was called 'imp' but here we have always 2 images
    :param start_params: DetectionThresholdRange variable
    :param filament_width: the width in pixels of the lines, selected via GUI.
    :param mask_width: mask_width in pixels
    :param global_runs:
    :param local_runs:
    :param normalize:
    :return:
    """

    enhanced_substack = get_enhanced_substack(substack=substack,filament_width=filament_width, mask_width=mask_width ,numberOfProcessors=cpu_count())

    if start_params is None:
        start_params=searchRange2(imgs=enhanced_substack,filament_width=filament_width)


    return ridge_optimizer_worker(enhanced_substack=enhanced_substack, binary_substack=get_binary_substack(substack=substack),
                                  start_params=start_params, filament_width=filament_width, global_runs=global_runs,
                                  local_runs=local_runs,normalize=normalize)


# Parallel_Ridge_OptimizerWorwer.java functions
def nextParamterSet(detection_threshold_range):
    #todo: implement it
    return True

def getGoodness(enhanced_images, binary_stack, detection_context,normalize):
    # todo: implement it
    return 0.1

def ridge_optimizer_worker(enhanced_substack, binary_substack, start_params, filament_width, global_runs,local_runs,normalize):
    sigma=filamentWidthToSigma(filament_width)
    best_detection_para=start_params # or None ???
    best_goodness = -10000000
    for i in range(global_runs+local_runs+1):
        if i>global_runs:
            candidate_range=nextParamterSet(start_params)
        else:
            value_range = -0.0075*(i-local_runs)+0.5
            candidate_range = nextParamterSet(createDetectionThresholdRange(lower_threshold=best_detection_para["lower_threshold"]*value_range,
                                                                            upper_threshold=best_detection_para["lower_threshold"]*(1+value_range)))

        goodness= getGoodness(enhanced_images=enhanced_substack, binary_stack=binary_substack,
                              detection_context=createFilamentDetectorContext(sigma=sigma, lower_threshold=candidate_range["lower_threshold"],upper_threshold=candidate_range["lower_threshold"]),
                                                                              normalize=normalize)

        # todo: check it better
        if goodness>best_goodness:
            best_goodness=goodness
            best_detection_para=candidate_range

    return best_detection_para
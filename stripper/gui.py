"""
MIT License

Copyright (c) 2020 Max Planck Institute of Molecular Physiology

Author: Luca Lusnig (luca.lusnig@mpi-dortmund.mpg.de)
Author: Thorsten Wagner (thorsten.wagner@mpi-dortmund.mpg.de)
Author: Markus Stabrin (markus.stabrin@mpi-dortmund.mpg.de)
Author: Fabian Schoenfeld (fabian.schoenfeld@mpi-dortmund.mpg.de)
Author: Tapu Shaikh (tapu.shaikh@mpi-dortmund.mpg.de)
Author: Adnan Ali (adnan.ali@mpi-dortmund.mpg.de)


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from stripper.run import run
from stripper.gui_helper import click_exit,show_info,files_in_dir,gui_files_in_dir,gen_id

from os import path
from tkinter import Tk,Label,Button,Entry,Checkbutton,BooleanVar,StringVar,filedialog,scrolledtext,INSERT,messagebox, IntVar,DoubleVar


def create_win():
    # create the window
    window = Tk()
    window.title("Collect parameter ")
    window.geometry("750x650")
    history=list()
    values = {"img":None, "enhanced_imgs_no_equalized":None,"enhanced_imgs_equalized":None,"lines_in_enhanced_substack":None}
    last_commands=None

    # collect the input param and call the run function
    def clicked():
        if path_to_file.get()=="":
            messagebox.showinfo("ERROR","Select an input file ")
        customMask= custom_mask.get()
        if path.isfile(customMask) is False:
            customMask = False
        detection_param= {"Detection":{'Filament_width': float(filament_width.get()), 'Lower_Threshold': float(lower_Threshold.get()), 'Upper_Threshold': float(upper_Threshold.get()), 'Mask_width': int(mask_width.get()), 'Equalize': equalize_state.get()}}
        filtering_param={"Filtering":{'Min_number_of_boxes': min_number_of_boxes.get(), 'Sigma_min_response': float(sigma_min_response.get()),
                                      'Sigma_max_response': float(sigma_max_response.get()), 'Sensitivity': float(sensitivity.get()), 'Min_straightness': float(min_straightness.get()),
                                      'Window_size': window_size.get(), 'Allowed_box_overlapping': float(allowed_box_overlapping.get()), 'Custom_mask':customMask}}
        general_param={"General":{'Box_size': box_size.get(), 'Box_distance': box_distance.get(), 'place_points': place_points_state.get(), 'Convert_to_8bit': convert_to_8bit_state.get(), 'Resize': int(resize.get())}}
        cmd = {"path_to_file": path_to_file.get()}
        cmd.update(detection_param)
        cmd.update(filtering_param)
        cmd.update(general_param)

        run(param=cmd,darkline=dark_line_state.get(),overlap=overlap_state.get(),save_out_on_disk=save_on_disk_state.get())


    def clicked_assistent():
        pass


    # grid for colletting params
    id_row=gen_id()
    Label(window, text="path_to_file").grid(column=0, row=id_row.current())
    path_to_file = Entry(window, width=30)
    path_to_file.grid(column=1, row=id_row.current())
    def clicked_file():
        path_to_file.delete(0, 'end')
        selected_file = filedialog.askopenfilenames(initialdir='.')
        path_to_file.insert(0, selected_file)
    def reset_file():
        path_to_file.delete(0, 'end')
    Button(window, text="Set file", command=clicked_file).grid(column=2, row=id_row.current())
    Button(window, text="reset file", command=reset_file).grid(column=3, row=id_row.current())

    row_asked_Dir = id_row.next()
    Label(window, text="path_to_directory").grid(column=0, row=row_asked_Dir)
    txt_askedDir = Entry(window, width=30)
    txt_askedDir.grid(column=1, row=row_asked_Dir)
    def clicked_dir():
        selected_dir = filedialog.askdirectory(initialdir='/')
        txt_askedDir.insert(0, selected_dir)
        #gui_files_in_dir(selected_dir)     # show the params in a new window
        # the following 3 lines show the list of files in the main windows
        txt_list_files = scrolledtext.ScrolledText(window, width=30, height=5)
        txt_list_files.insert(INSERT, files_in_dir(selected_dir))
        txt_list_files.grid(column=3, row=row_asked_Dir)
        messagebox.showinfo('Set directory', "option not implemented")
    Button(window, text="Set directory", command=clicked_dir).grid(column=2, row=row_asked_Dir)


    # Assistent
    id_row.next()
    Button(window, text="Assistent", command=clicked_assistent).grid(column=1, row=id_row.next())


    # Detectio params
    ### creating the var where for storaging the values
    filament_width=StringVar(value="8.0")
    lower_Threshold = StringVar(value="0.6")
    upper_Threshold = StringVar(value="3.0")
    mask_width = IntVar(value="100")

    Label(window, text="").grid(column=0, row=id_row.next())
    Label(window, text="Detection:").grid(column=0, row=id_row.next())
    Label(window, text="Filament_width").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=filament_width).grid(column=1, row=id_row.current())
    Label(window, text="Lower_Threshold").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=lower_Threshold).grid(column=1, row=id_row.current())
    Label(window, text="Upper_Threshold").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=upper_Threshold).grid(column=1, row=id_row.current())
    Label(window, text="Mask_width").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=mask_width).grid(column=1, row=id_row.current())
    equalize_state = BooleanVar()
    equalize_state.set(True)  # set check state
    equalize = Checkbutton(window, text='Equalize', var=equalize_state)
    equalize.grid(column=1, row=id_row.next())
    dark_line_state = BooleanVar()
    dark_line = Checkbutton(window, text='DarkLine', var=dark_line_state)
    dark_line.grid(column=1, row=id_row.next())
    overlap_state = BooleanVar()
    overlap = Checkbutton(window, text='Overlap ', var=overlap_state)
    overlap.grid(column=1, row=id_row.next())


    # Filtering params
    ### creating the var where for storaging the values
    min_number_of_boxes = IntVar(value="7")
    sigma_min_response = StringVar(value="0.0")
    sigma_max_response = StringVar(value="0.0")
    min_straightness = StringVar(value="0.0")
    sensitivity = StringVar(value="0.9")
    window_size = IntVar(value="30")
    allowed_box_overlapping = StringVar(value="0.5")

    Label(window, text="").grid(column=0, row=id_row.next())
    Label(window, text="\nFiltering:").grid(column=0, row=id_row.next())
    Label(window, text="Min_number_of_boxes").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=min_number_of_boxes).grid(column=1, row=id_row.current())
    Label(window, text="Sigma_min_response").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=sigma_min_response).grid(column=1, row=id_row.current())
    Label(window, text="Sigma_max_response").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=sigma_max_response).grid(column=1, row=id_row.current())
    Label(window, text="Sensitivity").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=sensitivity).grid(column=1, row=id_row.current())
    Label(window, text="Min_straightness").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=min_straightness).grid(column=1, row=id_row.current())
    Label(window, text="Window_size").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=window_size).grid(column=1, row=id_row.current())
    Label(window, text="Allowed_box_overlapping").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=allowed_box_overlapping).grid(column=1, row=id_row.current())
    Label(window, text="custom_mask").grid(column=0, row=id_row.next())
    custom_mask = Entry(window, width=30,textvariable=StringVar(value="None"))
    custom_mask.grid(column=1, row=id_row.current())


    def clicked_file_mask():
        custom_mask.delete(0, 'end')
        selected_file = filedialog.askopenfilenames(initialdir='.')
        custom_mask.insert(0, selected_file)
    def reset_file_mask():
        custom_mask.delete(0, 'end')
        custom_mask.insert(0, "None")
    Button(window, text="Set file", command=clicked_file_mask).grid(column=2, row=id_row.current())
    Button(window, text="reset file", command=reset_file_mask).grid(column=3, row=id_row.current())


    # General params
    ### creating the var where for storaging the values
    box_size = IntVar(value="64")
    box_distance = IntVar(value="10")
    resize = StringVar(value="1024")

    Label(window, text="").grid(column=0, row=id_row.next())
    Label(window, text="\nGeneral:").grid(column=0, row=id_row.next())
    Label(window, text="Box_size").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=box_size).grid(column=1, row=id_row.current())
    Label(window, text="Box_distance").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=box_distance).grid(column=1, row=id_row.current())
    place_points_state = BooleanVar()
    place_points = Checkbutton(window, text='Place_points', var=place_points_state)
    place_points.grid(column=1, row=id_row.next())
    convert_to_8bit_state = BooleanVar()
    convert_to_8bit_state.set(True)  # set check state
    convert_to_8bit = Checkbutton(window, text='Convert_to_8bit', var=convert_to_8bit_state)
    convert_to_8bit.grid(column=1, row=id_row.next())
    Label(window, text="Resize").grid(column=0, row=id_row.next())
    Entry(window, width=10,textvariable=resize).grid(column=1, row=id_row.current())
    save_on_disk_state = BooleanVar()
    save_on_disk_state.set(True)  # set check state
    save_on_disk = Checkbutton(window, text='Save on disk', var=save_on_disk_state)
    save_on_disk.grid(column=1, row=id_row.next())

    Label(window, text="").grid(column=0, row=id_row.next())
    Label(window, text="").grid(column=0, row=id_row.next())

    Button(window, text="send parameter",command=clicked).grid(column=0, row=id_row.next())
    Button(window, text="Info parameters", command=show_info).grid(column=2, row=id_row.current())

    Label(window, text="").grid(column=0, row=id_row.next())
    Button(window, text="EXIT", command=click_exit).grid(column=2, row=id_row.next())
    window.mainloop()
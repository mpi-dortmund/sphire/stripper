"""
MIT License

Copyright (c) 2020 Max Planck Institute of Molecular Physiology

Author: Luca Lusnig (luca.lusnig@mpi-dortmund.mpg.de)
Author: Thorsten Wagner (thorsten.wagner@mpi-dortmund.mpg.de)
Author: Markus Stabrin (markus.stabrin@mpi-dortmund.mpg.de)
Author: Fabian Schoenfeld (fabian.schoenfeld@mpi-dortmund.mpg.de)
Author: Tapu Shaikh (tapu.shaikh@mpi-dortmund.mpg.de)
Author: Adnan Ali (adnan.ali@mpi-dortmund.mpg.de)


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from tkinter import Tk, messagebox,Button, Label,scrolledtext,INSERT,Canvas,Toplevel,BOTH
from os import path, listdir
from PIL import Image,ImageTk

class gen_id(object):
    """
    Generator, that returns the current row for the grid alignment
    """
    def __init__(self):
        self.row=0
    def __next__(self):
        return self.next()
    def next(self):
        self.row=self.row+1
        return self.row
    def current(self):
        return self.row


def files_in_dir(directory):
    """
    given the path to a directory returns the list of the files in it as single string
    :param directory: path to directory to analyze
    :return: string containing the list of the files
    """
    out=""
    onlyfiles = [f for f in listdir(directory) if path.isfile(path.join(directory, f))]
    for f in onlyfiles:
        out+=f+"\n"
    return out


def gui_files_in_dir(directory):
    """
    given the path to a directory shows the list of the files in a window
    :param directory:
    :return:
    """
    window_list = Tk()
    window_list.title("files in "+directory)
    window_list.geometry("300x300")
    txt_list_files = scrolledtext.ScrolledText(window_list, width=30, height=10)
    txt_list_files.insert(INSERT, files_in_dir(directory))
    txt_list_files.grid(column=0, row=0)
    window_list.mainloop()

def click_exit():
    exit()


def show_images(rd_out,striper_out,save_out_on_disk,used_mask):
    """
    Show the RD output image plotted on the enhanced image and the striper out.
    I had to use the workaround to be able to show both the images
        https://stackoverflow.com/questions/43552320/python-can-i-open-two-tkinter-windows-at-the-same-time/43552391
    :param rd_out: numpy of the ridge detection output
    :param striper_out: numpy of the script's output
    :param save_out_on_disk: if True save on disk the images as 'out_striper.png' and 'out_ridge_detection.png' on the current folder otherwise just show them
    :param used_mask: True if a mask was applied. It is just to write the titleof the window
    :return:
    """
    root = Tk()
    root.title("ridge detection output on the enhanced image")
    root.geometry(str(rd_out.shape[0]) + 'x' + str(rd_out.shape[1]))

    canvas = Canvas(root, width=rd_out.shape[0], height=rd_out.shape[1])
    canvas.pack()
    img1 = Image.fromarray(rd_out)
    if save_out_on_disk:
        img1.save("out_ridge_detection.png")
    rd_img = ImageTk.PhotoImage(master=canvas, image=img1)
    canvas.create_image(0, 0, anchor="nw", image=rd_img)

    def toplevel():
        top = Toplevel()
        title = "Striper output" if used_mask is False else "Striper output (a mask was applied)"
        top.title(title)
        top.wm_geometry(str(rd_out.shape[0]) + 'x' + str(rd_out.shape[1]))
        canvas2 = Canvas(top)
        canvas2.pack(fill=BOTH, expand=1)
        img2 = Image.fromarray(striper_out)
        if save_out_on_disk:
            img2.save("out_striper.png")
        stripper_img = ImageTk.PhotoImage(master=canvas2, image=img2)
        canvas2.create_image(0, 0, anchor="nw", image=stripper_img)
        canvas2.create_image(0, 0, anchor="nw", image=rd_img)

    toplevel()
    root.mainloop()
# message box info for the params
def click_info_setdir():
    messagebox.showinfo("Set directory","Select the folder containing the micrographs to be analyzed")
def click_info_setfile():
    messagebox.showinfo("Set file","Select the micrograph to analyze")

def click_info_box_size():
    messagebox.showinfo("Box_size","Size the boxes in pixel")
def click_info_box_distance():
    messagebox.showinfo("Box_distance","Distance in pixel between two boxes along the filament.")
def click_info_convert_to_8bit():
    messagebox.showinfo("Convert_to_8bit","Convert the input image in an 8 bit image")
def click_info_resize():
    messagebox.showinfo("Resize","Resize image as a square image of given size")
def click_info_filament():
    messagebox.showinfo("Filament_width","The width of your filaments in pixel")
def click_info_threshold_lw():
    messagebox.showinfo("Lower_Threshold","Filter responses lower than that value will be considered as background and filament tracing stops here.\nHigher values lead to more segmented lines, as low contrast regions of a filament will be considered as background the line is splitted at this position")
def click_info_threshold_up():
    messagebox.showinfo("Upper_Threshold","Filter responens higher than that will be considered as valid filament starting point.\nWith higher values the contrast of the filaments have to higher be to be picked")
def click_info_mask_width():
    messagebox.showinfo("Mask_width","As the response along the filament randomly fluctuates and sometimes disappear, it is important to average along the filament")
def click_info_equalize():
    messagebox.showinfo("Equalize","After enhancement of the filaments, all images are adjusted that filamants have a more similar absolute response. Especially when the image contains high contrast contaminations, this option has a positive effect.\n\nBy defaut this option is activated.")
def click_info_darkline():
    messagebox.showinfo("DarkLine","Determines whether dark or bright lines are extracted by the ridge detection tool.")
def click_info_overlap():
    messagebox.showinfo("Overlap resolution"," You can select a method to attempt automatic overlap resolution. The accuracy of this method will depend on the structure of your data.")
def click_info_min_number_of_boxes():
    messagebox.showinfo("Min_number_of_boxes","The minimum number of boxes that have to be placed per line")
def click_info_sigma_min_response():
    messagebox.showinfo("Sigma_min_response","The mean response M and the standard deviation S are estimated over all lines per image.\nIf a line has a signficant number of line points below the threshold \nT = M-F*S it is considered as false-positiv (background) and is removed, whereas F is value to specifiy.\n\nBy default, this filter is deactivated (set to 0). A resonable value lies typically between 2 - 3.")
def click_info_sigma_max_response():
    messagebox.showinfo("Sigma_max_response","The mean response M and the standard deviation S are estimated over all lines per image.\nIf a line has a signficant number of line points above the threshold \nT = M+F*S it is considered as false-positiv (ice,overlapping filament) and is removed, whereas F is value to specifiy.\n\nBy default, this filter is deactivated (set to 0). A resonable value lies typically between 2 - 3.")
def click_info_sensitivity():
    messagebox.showinfo("Sensitivity","Relative amount to what extend two boxes auf different filaments are allowed to overlap.\n\nThe default value 0.5 which is the maximum value that ensures that a box contains only one filament")
def click_info_min_straightness():
    messagebox.showinfo("Min_straightness","Threshold value for minimum straightness. Line segments (see window size) with a straightness below that threshold will be removed and the line is splitted.\nHigher values means that the filament have to be more straight, where 1 means a perfect straight line")
def click_info_window_size():
    messagebox.showinfo("Window_size","The number of line points which are used to estimate the straightness. Larger values lead to more robustness against noisebut decrease to ability to detect small curves")
def click_info_allowed_box_overlapping():
    messagebox.showinfo("Allowed_box_overlapping","Relative amount to what extend two boxes auf different filaments are allowed to overlap.\n\nThe default value 0.5 which is the maximum value that ensures that a box contains only one filament")
def click_info_custom_mask():
    messagebox.showinfo("Custom_mask","mask image.\nHAS TO BE A BINARY IMAGE. The values to mask have to be set as black")
def click_info_save_on_disk():
    messagebox.showinfo("Save on disk","Save the ridge detection on the enhanced image, 'out_ridge_detection.png',  and the final output, 'out_striper.png', on the disk in the current folder")
def click_info_assistent():
    messagebox.showinfo("Assistent","TODO")


def show_info():
    """
    It creates the info windows. each botton open a message info about the selected parameter
    :return:
    """
    id_row = gen_id()
    id_col_detection = gen_id()
    id_col_filtering = gen_id()
    id_col_general = gen_id()
    window_info = Tk()
    window_info.title("Parameters information")
    window_info.geometry('1200x275')

    Label(window_info, text="Selection micrograph[s] Parameters:").grid(column=0, row=id_row.current())
    Button(window_info, text="Set directory", command=click_info_setdir).grid(column=0, row=id_row.next())
    Button(window_info, text="Set file", command=click_info_setfile).grid(column=1, row=id_row.current())

    Label(window_info).grid(column=0, row=id_row.next())
    Label(window_info, text="Assistent:").grid(column=0, row=id_row.next())
    Button(window_info, text="Assistent", command=click_info_assistent).grid(column=0, row=id_row.next())

    Label(window_info).grid(column=id_col_detection.current(), row=id_row.next())
    Label(window_info, text="Detection Parameters:").grid(column=id_col_detection.current(), row=id_row.next())
    Button(window_info, text="Filament_width", command=click_info_filament).grid(column=id_col_detection.current(), row=id_row.next())
    Button(window_info, text="Lower_Threshold", command=click_info_threshold_lw).grid(column=id_col_detection.next(), row=id_row.current())
    Button(window_info, text="Upper_Threshold", command=click_info_threshold_up).grid(column=id_col_detection.next(), row=id_row.current())
    Button(window_info, text="Mask_width", command=click_info_mask_width).grid(column=id_col_detection.next(), row=id_row.current())
    Button(window_info, text="Equalize", command=click_info_equalize).grid(column=id_col_detection.next(), row=id_row.current())
    Button(window_info, text="DarkLine", command=click_info_darkline).grid(column=id_col_detection.next(), row=id_row.current())
    Button(window_info, text="Overlap", command=click_info_overlap).grid(column=id_col_detection.next(), row=id_row.current())

    Label(window_info).grid(column=id_col_filtering.current(), row=id_row.next())
    Label(window_info, text="Filtering Parameters:").grid(column=id_col_filtering.current(), row=id_row.next())
    Button(window_info, text="Min_number_of_boxes", command=click_info_min_number_of_boxes).grid(column=id_col_filtering.current(), row=id_row.next())
    Button(window_info, text="Sigma_min_response", command=click_info_sigma_min_response).grid(column=id_col_filtering.next(), row=id_row.current())
    Button(window_info, text="Sigma_max_response", command=click_info_sigma_max_response).grid(column=id_col_filtering.next(), row=id_row.current())
    Button(window_info, text="Sensitivity", command=click_info_sensitivity).grid(column=id_col_filtering.next(), row=id_row.current())
    Button(window_info, text="Min_straightness", command=click_info_min_straightness).grid(column=id_col_filtering.next(), row=id_row.current())
    Button(window_info, text="Window_size", command=click_info_window_size).grid(column=id_col_filtering.next(), row=id_row.current())
    Button(window_info, text="Allowed_box_overlapping", command=click_info_allowed_box_overlapping).grid(column=id_col_filtering.next(), row=id_row.current())
    Button(window_info, text="Custom_mask", command=click_info_custom_mask).grid(column=id_col_filtering.next(), row=id_row.current())

    Label(window_info).grid(column=id_col_general.current(), row=id_row.next())
    Label(window_info, text="General Parameters:").grid(column=id_col_general.current(), row=id_row.next())
    Button(window_info, text="Box_size", command=click_info_box_size).grid(column=id_col_general.current(), row=id_row.next())
    Button(window_info, text="Box_distance", command=click_info_box_distance).grid(column=id_col_general.next(), row=id_row.current())
    Button(window_info, text="Convert_to_8bit", command=click_info_convert_to_8bit).grid(column=id_col_general.next(), row=id_row.current())
    Button(window_info, text="Resize", command=click_info_resize).grid(column=id_col_general.next(), row=id_row.current())
    Button(window_info, text="Save on disk", command=click_info_save_on_disk).grid(column=id_col_general.next(), row=id_row.current())

    window_info.mainloop()
from stripper.gui_helper import gen_id,Tk
from tkinter import Label,Button,Entry,Canvas
from PIL import Image, ImageTk
from math import sqrt
from numpy import asarray

#todo: it is just a template. You have to rewrite it entirely

""" global variables necessary for exchanging variable among the windows """

#
width_assistant =0

# for detecting the width
x_width=[]
y_width=[]
again_select_width=True


def assistant(path_dir):
    """
    The assistant for semi automatic calculation of the ridge detection params
    :param path_dir: path to the directory
    :return:
    """
    global width_assistant

    def close_win():
        assistent_win.quit()        # exit to the loop but do not close the window, if I use 'destroy I lose the width.get() value

    def set_width():
        global width_assistant
        img=asarray(Image.open("enhanced_img.tif"))
        width_assistant = select_width(img)

    assistent_win = Tk()
    assistent_win.title("width_assistant ")
    assistent_win.geometry("500x500")

    id_row = gen_id()
    btn_quit = Button(assistent_win, text="Set width", command=set_width)
    btn_quit.grid(column=0, row=id_row.next())


    btn_quit = Button(assistent_win, text="QUIT", command=close_win)
    btn_quit.grid(column=0, row=id_row.next())
    assistent_win.mainloop()


    assistent_win.destroy()     # I'm here after closing the loop and got the inserted values hence I can destroy the window





    def select_width(img):
        """
        Path of the img
        Click twice the left button to display the line.
        Click a third time left to confirm, otherwise right and select the line from scratch
        :param img: numpy array
        :return: the width of the line
        """
        global x_width, y_width, again_select_width

        def reset_coordinate(event):
            """ In case of left click of the mouse it restart the loop resetting the list of the collected coordinate"""
            y_width.clear()
            x_width.clear()
            print("Reset the coordinates")
            root_select_width.destroy()

        def get_coordinate(event):
            """
            For each right click of the mouse it add the coordinate of the mouse pointer
            The second click show the selected line. The third one exit the loop
            """
            global again_select_width
            x_width.append(event.x)
            y_width.append(event.y)
            if len(x_width) ==2:
                canvas2.create_line(x_width[0], y_width[0], x_width[1], y_width[1], fill='red')
            if len(x_width)==3:
                x_width.pop()
                y_width.pop()
                again_select_width=False
                root_select_width.quit()

        img = Image.fromarray(img)

        #loop the creation of the window to be able to reset the selected clicked points
        while again_select_width is True:
            root_select_width = Tk()

            canvas2 = Canvas(width=img.size[0], height=img.size[1])
            canvas2.pack()
            photo = ImageTk.PhotoImage(img)
            canvas2.create_image(0,0, anchor="nw",image=photo)       # with anchor the image will fit in the good way the canvas


            canvas2.bind('<Button-1>', get_coordinate)
            canvas2.bind("<Button-3>",reset_coordinate)
            root_select_width.mainloop()
            #root_select_width.destroy()

        return int(sqrt( (x_width[0]-x_width[1])*(x_width[0]-x_width[1])  + (y_width[0]-y_width[1])*(y_width[0]-y_width[1]) ))

    return width_assistant